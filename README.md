ROCKOUT(1) - General Commands Manual

# NAME

**rockout** - a front-end & launcher for mpv written in POSIX shell

# SYNOPSIS

**rockout**
\[**-dhlpqrs**]
*'expression'*

# DESCRIPTION

The
**rockout**
script uses shell utilities and mpv to generate a plain-text music library, search it using extended regular expressions, and play selections in your console.

**rockout**
searches $MUSIC\_DIR, which is ${HOME}/music by default, for all files that are not image files (JPG, PNG, or GIF) to build a music library that isn't cluttered with cover art files.
The library, once generated with
**-l**
is located at ${HOME}/.local/rockout/library.m3u. Since library.m3u uses absolute file paths, it is compatible with other applications that use M3U playlists.

Because
**rockout**
uses
**grep -E**
in its
**egrep**
alias, it can query its library with grep-compatible extended regular expressions.

By default,
**rockout**
provides a count of tracks found for
*'expression'*
, saves the result as /tmp/rockout-${USERNAME}.m3u, and immediately loads the playlist into mpv. However, by using the
**-q**
option, the operator can do a dry run. The operator can also use the
**-p**
option to reuse the previous playlist.

Once
**rockout**
has generated a playlist and piped it to mpv, the operator can control playback using mpv's keybindings.

**rockout**
can handle any music format supported by mpv; it doesn't matter if your music directory contains MP3, MP4, FLAC, Ogg Vorbis, Ogg Opus, MIDI, or M3U files.

The options are as follows:

**-d**

> force mpv to run in windowed mode and show album art when available

**-h**

> dump a brief tutorial to the console

**-l**

> generate a plain-text music library

**-p**

> use the previously generated playlist located in $PLAYLIST

**-q**

> search the library for tracks matching
> *'expression'*
> and send results to standard output

**-r**

> repeat/loop playlist

**-s**

> shuffle playlist during playback

# ENVIRONMENT

MUSIC\_DIR

> the directory in which the operator keeps their music collection. This defaults to ${HOME}/music based on the author's setup.

PLAYLIST

> the file where
> **rockout**
> keeps the results of the previous search. This is /tmp/rockout-${USERNAME}.m3u by default, and the permissions are set to 600 to provide a modicum of privacy.

# FILES

.local/rockout/library.m3u

> a standard M3U file representing one's entire music collection

/tmp/rockout-${USERNAME}.m3u

> a standard M3U file containing cached search results

# EXIT STATUS

The **rockout** utility exits&#160;0 on success, and&#160;&gt;0 if an error occurs.

# EXAMPLES

Generate your library after installation:

	$ rockout -l

Generate your library after installation on GNU/Linux or GNU/systemd:

	$ MUSIC_DIR=$HOME/Music rockout -l

Search for songs released in 1980:

	$ rockout -q 1980

Play songs by Frank Zappa in random order without looping:

	$ rockout -s Zappa

Loop a classic Judas Priest album:

	$ rockout -r Stained_Class

Put some classic Dio albums on repeat and shuffle:

	$ rockout -rs 'Dio.*Holy_Diver|Sacred_Heart|Dream_Evil'

# SEE ALSO

find(1)
grep(1)
egrep(1)
sort(1)
wc(1)
mpv(1)

# HISTORY

**rockout**
exists because the author got tired of writing ad-hoc find queries and piping those to mpv.

# AUTHORS

Matthew Graybosch &lt;[contact@matthewgraybosch.com](mailto:contact@matthewgraybosch.com)&gt;

# CAVEATS

**rockout**
depends on having a reasonably well-organized music collection at the filesystem level since it does not attempt to deal with ID3 tags or their equivalent in Ogg Vorbis and FLAC files. Its utility depends entirely on the operator's facility with extended regular expressions.

While the playlist file in /tmp/rockout-${USERNAME}.m3u is read-write for its owner and inaccessible to non-root users, root (and users with equivalent access via doas/sudo) can still read and alter the file on a shared system.

**rockout**
depends on mpv, which must be installed separately using your operating system's package manager.

# BUGS

**rockout**
is both less intelligent and more literal-minded than its author, and cannot tell a Dio album from a Dio tribute album unless provided a properly crafted search expression.

**rockout**
handles switches in the order provided. If the operator wants to use
**-p**
with switches that control the windowed, repeat, and shuffle modes, they should use
**-p**
last.

Should any other bugs occur, please report them to the author.

OpenBSD 6.9 - April 13, 2021
