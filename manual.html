<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8"/>
  <style>
    table.head, table.foot { width: 100%; }
    td.head-rtitle, td.foot-os { text-align: right; }
    td.head-vol { text-align: center; }
    .Nd, .Bf, .Op { display: inline; }
    .Pa, .Ad { font-style: italic; }
    .Ms { font-weight: bold; }
    .Bl-diag > dt { font-weight: bold; }
    code.Nm, .Fl, .Cm, .Ic, code.In, .Fd, .Fn, .Cd { font-weight: bold;
      font-family: inherit; }
  </style>
  <title>ROCKOUT(1)</title>
</head>
<body>
<table class="head">
  <tr>
    <td class="head-ltitle">ROCKOUT(1)</td>
    <td class="head-vol">General Commands Manual</td>
    <td class="head-rtitle">ROCKOUT(1)</td>
  </tr>
</table>
<div class="manual-text">
<section class="Sh">
<h1 class="Sh" id="NAME"><a class="permalink" href="#NAME">NAME</a></h1>
<p class="Pp"><code class="Nm">rockout</code> &#x2014; <span class="Nd">a
    front-end &amp; launcher for mpv written in POSIX shell</span></p>
</section>
<section class="Sh">
<h1 class="Sh" id="SYNOPSIS"><a class="permalink" href="#SYNOPSIS">SYNOPSIS</a></h1>
<table class="Nm">
  <tr>
    <td><code class="Nm">rockout</code></td>
    <td>[<code class="Fl">-dhlpqrs</code>]
      <var class="Ar">'expression'</var></td>
  </tr>
</table>
</section>
<section class="Sh">
<h1 class="Sh" id="DESCRIPTION"><a class="permalink" href="#DESCRIPTION">DESCRIPTION</a></h1>
<p class="Pp">The <code class="Nm">rockout</code> script uses shell utilities
    and mpv to generate a plain-text music library, search it using extended
    regular expressions, and play selections in your console.</p>
<p class="Pp"><code class="Nm">rockout</code> searches $MUSIC_DIR, which is
    ${HOME}/music by default, for all files that are not image files (JPG, PNG,
    or GIF) to build a music library that isn't cluttered with cover art files.
    The library, once generated with <code class="Fl">-l</code> is located at
    ${HOME}/.local/rockout/library.m3u. Since library.m3u uses absolute file
    paths, it is compatible with other applications that use M3U playlists.</p>
<p class="Pp">Because <code class="Nm">rockout</code> uses <code class="Nm">grep
    -E</code> in its <code class="Nm">egrep</code> alias, it can query its
    library with grep-compatible extended regular expressions.</p>
<p class="Pp">By default, <code class="Nm">rockout</code> provides a count of
    tracks found for <var class="Ar">'expression'</var> , saves the result as
    /tmp/rockout-${USERNAME}.m3u, and immediately loads the playlist into mpv.
    However, by using the <code class="Fl">-q</code> option, the operator can do
    a dry run. The operator can also use the <code class="Fl">-p</code> option
    to reuse the previous playlist.</p>
<p class="Pp">Once <code class="Nm">rockout</code> has generated a playlist and
    piped it to mpv, the operator can control playback using mpv's
  keybindings.</p>
<p class="Pp"><code class="Nm">rockout</code> can handle any music format
    supported by mpv; it doesn't matter if your music directory contains MP3,
    MP4, FLAC, Ogg Vorbis, Ogg Opus, MIDI, or M3U files.</p>
<p class="Pp">The options are as follows:</p>
<dl class="Bl-tag">
  <dt id="d"><a class="permalink" href="#d"><code class="Fl">-d</code></a></dt>
  <dd>force mpv to run in windowed mode and show album art when available</dd>
  <dt id="h"><a class="permalink" href="#h"><code class="Fl">-h</code></a></dt>
  <dd>dump a brief tutorial to the console</dd>
  <dt id="l"><a class="permalink" href="#l"><code class="Fl">-l</code></a></dt>
  <dd>generate a plain-text music library</dd>
  <dt id="p"><a class="permalink" href="#p"><code class="Fl">-p</code></a></dt>
  <dd>use the previously generated playlist located in $PLAYLIST</dd>
  <dt id="q"><a class="permalink" href="#q"><code class="Fl">-q</code></a></dt>
  <dd>search the library for tracks matching <var class="Ar">'expression'</var>
      and send results to standard output</dd>
  <dt id="r"><a class="permalink" href="#r"><code class="Fl">-r</code></a></dt>
  <dd>repeat/loop playlist</dd>
  <dt id="s"><a class="permalink" href="#s"><code class="Fl">-s</code></a></dt>
  <dd>shuffle playlist during playback</dd>
</dl>
</section>
<section class="Sh">
<h1 class="Sh" id="ENVIRONMENT"><a class="permalink" href="#ENVIRONMENT">ENVIRONMENT</a></h1>
<dl class="Bl-tag">
  <dt>MUSIC_DIR</dt>
  <dd>the directory in which the operator keeps their music collection. This
      defaults to ${HOME}/music based on the author's setup.</dd>
  <dt>PLAYLIST</dt>
  <dd>the file where <code class="Nm">rockout</code> keeps the results of the
      previous search. This is /tmp/rockout-${USERNAME}.m3u by default, and the
      permissions are set to 600 to provide a modicum of privacy.</dd>
</dl>
</section>
<section class="Sh">
<h1 class="Sh" id="FILES"><a class="permalink" href="#FILES">FILES</a></h1>
<dl class="Bl-tag Bl-compact">
  <dt>.local/rockout/library.m3u</dt>
  <dd>a standard M3U file representing one's entire music collection</dd>
  <dt>/tmp/rockout-${USERNAME}.m3u</dt>
  <dd>a standard M3U file containing cached search results</dd>
</dl>
</section>
<section class="Sh">
<h1 class="Sh" id="EXIT_STATUS"><a class="permalink" href="#EXIT_STATUS">EXIT
  STATUS</a></h1>
<p class="Pp">The <code class="Nm">rockout</code> utility exits&#x00A0;0 on
    success, and&#x00A0;&gt;0 if an error occurs.</p>
</section>
<section class="Sh">
<h1 class="Sh" id="EXAMPLES"><a class="permalink" href="#EXAMPLES">EXAMPLES</a></h1>
<p class="Pp">Generate your library after installation:</p>
<div class="Bd Pp Bd-indent Li">
<pre>$ rockout -l</pre>
</div>
<p class="Pp">Generate your library after installation on GNU/Linux or
    GNU/systemd:</p>
<div class="Bd Pp Bd-indent Li">
<pre>$ MUSIC_DIR=$HOME/Music rockout -l</pre>
</div>
<p class="Pp">Search for songs released in 1980:</p>
<div class="Bd Pp Bd-indent Li">
<pre>$ rockout -q 1980</pre>
</div>
<p class="Pp">Play songs by Frank Zappa in random order without looping:</p>
<div class="Bd Pp Bd-indent Li">
<pre>$ rockout -s Zappa</pre>
</div>
<p class="Pp">Loop a classic Judas Priest album:</p>
<div class="Bd Pp Bd-indent Li">
<pre>$ rockout -r Stained_Class</pre>
</div>
<p class="Pp">Put some classic Dio albums on repeat and shuffle:</p>
<div class="Bd Pp Bd-indent Li">
<pre>$ rockout -rs 'Dio.*Holy_Diver|Sacred_Heart|Dream_Evil'</pre>
</div>
</section>
<section class="Sh">
<h1 class="Sh" id="SEE_ALSO"><a class="permalink" href="#SEE_ALSO">SEE
  ALSO</a></h1>
<p class="Pp"><a class="Xr">find(1)</a> <a class="Xr">grep(1)</a>
    <a class="Xr">egrep(1)</a> <a class="Xr">sort(1)</a> <a class="Xr">wc(1)</a>
    <a class="Xr">mpv(1)</a></p>
</section>
<section class="Sh">
<h1 class="Sh" id="HISTORY"><a class="permalink" href="#HISTORY">HISTORY</a></h1>
<p class="Pp"><code class="Nm">rockout</code> exists because the author got
    tired of writing ad-hoc find queries and piping those to mpv.</p>
</section>
<section class="Sh">
<h1 class="Sh" id="AUTHORS"><a class="permalink" href="#AUTHORS">AUTHORS</a></h1>
<p class="Pp"><span class="An">Matthew Graybosch</span>
    &lt;<a class="Mt" href="mailto:contact@matthewgraybosch.com">contact@matthewgraybosch.com</a>&gt;</p>
</section>
<section class="Sh">
<h1 class="Sh" id="CAVEATS"><a class="permalink" href="#CAVEATS">CAVEATS</a></h1>
<p class="Pp"><code class="Nm">rockout</code> depends on having a reasonably
    well-organized music collection at the filesystem level since it does not
    attempt to deal with ID3 tags or their equivalent in Ogg Vorbis and FLAC
    files. Its utility depends entirely on the operator's facility with extended
    regular expressions.</p>
<p class="Pp">While the playlist file in /tmp/rockout-${USERNAME}.m3u is
    read-write for its owner and inaccessible to non-root users, root (and users
    with equivalent access via doas/sudo) can still read and alter the file on a
    shared system.</p>
<p class="Pp"><code class="Nm">rockout</code> depends on mpv, which must be
    installed separately using your operating system's package manager.</p>
</section>
<section class="Sh">
<h1 class="Sh" id="BUGS"><a class="permalink" href="#BUGS">BUGS</a></h1>
<p class="Pp"><code class="Nm">rockout</code> is both less intelligent and more
    literal-minded than its author, and cannot tell a Dio album from a Dio
    tribute album unless provided a properly crafted search expression.</p>
<p class="Pp"><code class="Nm">rockout</code> handles switches in the order
    provided. If the operator wants to use <code class="Fl">-p</code> with
    switches that control the windowed, repeat, and shuffle modes, they should
    use <code class="Fl">-p</code> last.</p>
<p class="Pp">Should any other bugs occur, please report them to the author.</p>
</section>
</div>
<table class="foot">
  <tr>
    <td class="foot-date">April 13, 2021</td>
    <td class="foot-os">OpenBSD 6.9</td>
  </tr>
</table>
</body>
</html>
