PREFIX=/usr/local

.DEFAULT: help

help:
	@echo "Since rockout is a shell script, there's no build step. Just run \"make install\" with root privileges for system-wide installation to ${PREFIX}. If you prefer a different location, run \"make PREFIX=<alternate path> install\". You can also run \"make test\" to verify the install destination and \"make manuals\" to convert the manual page to Markdown, HTML, and PDF formats." | fmt

install:
	install -m 755 rockout ${PREFIX}/bin/
	install -m 644 rockout.1 ${PREFIX}/man/man1/
	makewhatis ${PREFIX}/man

manuals:
	mandoc -Tmarkdown rockout.1 > README.md
	mandoc -Tpdf rockout.1 > manual.pdf
	mandoc -Thtml rockout.1 > manual.html

test:
	@echo "Install location is ${PREFIX}."
